package presentacion.vista;

import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.TipoContactoDTO;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaTipoContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private static VentanaTipoContacto INSTANCE;
	private JScrollPane spEtiqueta;
	private JButton btnNuevo;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JTable tablaTipoContacto;
	private DefaultTableModel modelTipoContacto;
	private  String[] tipoContactoColumna = {"Tipo contacto"};

	
	public static VentanaTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaTipoContacto(); 	
			return new VentanaTipoContacto();
		}
		else
			return INSTANCE;
	}
	
	private VentanaTipoContacto() 
	{
		super();
		setTitle("Tipo de Contacto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 479, 352);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		spEtiqueta = new JScrollPane();
		spEtiqueta.setBounds(50, 28, 350, 178);
		contentPane.add(spEtiqueta);
		
		modelTipoContacto = new DefaultTableModel(null,tipoContactoColumna);
		tablaTipoContacto = new JTable(modelTipoContacto);
		
		tablaTipoContacto.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaTipoContacto.getColumnModel().getColumn(0).setResizable(false);
		
		spEtiqueta.setViewportView(tablaTipoContacto);
		
		btnNuevo = new JButton("Nuevo");
		btnNuevo.setBounds(50, 251, 89, 23);
		contentPane.add(btnNuevo);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(182, 251, 89, 23);
		contentPane.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBorrar.setBounds(311, 251, 89, 23);
		contentPane.add(btnBorrar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void llenarTablaTipoContacto(List<TipoContactoDTO> tipoContactoEnTabla)
	{
		this.getModelEtiqueta().setRowCount(0);
		this.getModelEtiqueta().setColumnCount(0);
		this.getModelEtiqueta().setColumnIdentifiers(this.getNombreEtiquetaColumna());

		for (TipoContactoDTO t : tipoContactoEnTabla)
		{
			String tipoContacto = t.getTipoContacto();
			
			Object[] fila = {tipoContacto};
			this.getModelEtiqueta().addRow(fila);
		}
	}

	private String[] getNombreEtiquetaColumna() {
		
		return this.tipoContactoColumna;
	}

	private DefaultTableModel getModelEtiqueta() {
		
		return this.modelTipoContacto;
	}

	public JButton getBtnNuevo() 
	{
		return btnNuevo;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JTable getTablaTipoContacto()
	{
		return tablaTipoContacto;
	}


}