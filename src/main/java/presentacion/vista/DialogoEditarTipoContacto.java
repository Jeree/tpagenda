package presentacion.vista;


import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.ValidadorInput;

public class DialogoEditarTipoContacto extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreTipoContacto;
	
	private JButton btnGuardarTipoContacto;
	private JButton btnCancelar;
	
	private ArrayList<String> campos;
	private ValidadorInput validador;
	
	private static DialogoEditarTipoContacto INSTANCE;
	
	public static DialogoEditarTipoContacto getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new DialogoEditarTipoContacto(); 	
			return new DialogoEditarTipoContacto();
		}
		else
			return INSTANCE;
	}
	
	private DialogoEditarTipoContacto() 
	{
		super();
		setTitle("Editar Tipo Contacto");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 590, 293);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(22, 25, 525, 192);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreTipoContacto = new JLabel("Nombre de la etiqueta: ");
		lblNombreTipoContacto.setBounds(84, 54, 145, 14);
		panel.add(lblNombreTipoContacto);
		
		txtNombreTipoContacto = new JTextField();
		
		txtNombreTipoContacto.setBounds(239, 51, 204, 20);
		panel.add(txtNombreTipoContacto);
		txtNombreTipoContacto.setColumns(10);
		
		btnGuardarTipoContacto = new JButton("Guardar Etiqueta");
		btnGuardarTipoContacto.setBounds(189, 110, 176, 28);
		panel.add(btnGuardarTipoContacto);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(189, 149, 176, 28);
		btnCancelar.addActionListener(e -> dispose());
		panel.add(btnCancelar);
		
		this.setVisible(false);
	}

	public ArrayList<String> camposInvalidos() {
		this.campos = new ArrayList<>();
		validador = new ValidadorInput();
		if(!validador.validarTextoConEspacios(txtNombreTipoContacto.getText())) campos.add("Nombre");
	
		return campos;
	}

	public String mensajeVerificarCampos() {
		return "Por favor revise el campo Nombre";
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTipoContacto() 
	{
		return txtNombreTipoContacto;
	}
	
	public JButton getBtnGuardarTipoContacto() 
	{
		return btnGuardarTipoContacto;
	}
	
	public void cerrar()
	{
		this.txtNombreTipoContacto.setText(null);
		this.dispose();
	}

	public void precargarDatos(String tipoContacto) {
		
		this.txtNombreTipoContacto.setText(tipoContacto);
		
	}
}