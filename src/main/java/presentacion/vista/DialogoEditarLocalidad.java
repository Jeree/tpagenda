package presentacion.vista;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.ValidadorInput;

public class DialogoEditarLocalidad extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombreLocalidad;
	
	private JButton btnGuardarLocalidad;
	private JButton btnCancelar;
	
	private ArrayList<String> campos;
	private ValidadorInput validador;
	
	private static DialogoEditarLocalidad INSTANCE;
	
	public static DialogoEditarLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new DialogoEditarLocalidad(); 	
			return new DialogoEditarLocalidad();
		}
		else
			return INSTANCE;
	}
	
	private DialogoEditarLocalidad() 
	{
		super();
		setTitle("Editar Localidad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 590, 293);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(22, 25, 525, 192);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreLocalidad = new JLabel("Nombre de la localidad: ");
		lblNombreLocalidad.setBounds(84, 54, 145, 14);
		panel.add(lblNombreLocalidad);
		
		txtNombreLocalidad = new JTextField();
		txtNombreLocalidad.setBounds(239, 51, 204, 20);
		panel.add(txtNombreLocalidad);
		txtNombreLocalidad.setColumns(10);
		
		btnGuardarLocalidad = new JButton("Guardar localidad");
		btnGuardarLocalidad.setBounds(189, 110, 176, 28);
		panel.add(btnGuardarLocalidad);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(189, 149, 176, 28);
		btnCancelar.addActionListener(e -> dispose());
		panel.add(btnCancelar);
		
		this.setVisible(false);
	}
		
	public ArrayList<String> camposInvalidos() {
		this.campos = new ArrayList<>();
		validador = new ValidadorInput();
		if(!validador.validarTextoConEspacios(txtNombreLocalidad.getText())) campos.add("Nombre");
	
		return campos;
	}

	public String mensajeVerificarCampos() {
		return "Por favor revise el campo Nombre";
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getLocalidad() 
	{
		return txtNombreLocalidad;
	}
	
	public JButton getBtnGuardarLocalidad() 
	{
		return btnGuardarLocalidad;
	}
	
	public void cerrar()
	{
		this.txtNombreLocalidad.setText(null);
		this.dispose();
	}

	public void precargarDatos(String localidad) {
		
		this.txtNombreLocalidad.setText(localidad);
		
	}
}