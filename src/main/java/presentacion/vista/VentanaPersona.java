package presentacion.vista;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import java.util.ArrayList;

import dto.LocalidadDTO;
import dto.TipoContactoDTO;
import presentacion.controlador.ValidadorInput;

import java.awt.Color;

import javax.swing.JComboBox;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDpto;
	private JTextField txtEmail;
	private JTextField txtTelefono;
	
	private JComboBox<String> localidad;
	private JComboBox<String> tipoContacto;
	private JDateChooser fechaCumple;
	
	private ValidadorInput validador;
	private ArrayList<String> campos;
	
	private JButton btnAgregarPersona;
	
	private static VentanaPersona INSTANCE;
	private JButton btnAgregarEtiqueta;
	private JButton btnAgregarLocalidad;
	private JButton btnCerrar;
	
	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() 
	{
		super();
		setTitle("Persona");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 574, 519);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 525, 452);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(90, 11, 113, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(90, 46, 113, 14);
		panel.add(lblApellido);
		
		JLabel lblCalle = new JLabel("Calle:");
		lblCalle.setBounds(90, 77, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura:");
		lblAltura.setBounds(90, 108, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso:");
		lblPiso.setBounds(90, 139, 113, 14);
		panel.add(lblPiso);
		
		JLabel lblDpto = new JLabel("Departamento:");
		lblDpto.setBounds(90, 167, 113, 14);
		panel.add(lblDpto);
		
		JLabel lblLocalidad = new JLabel("Localidad:");
		lblLocalidad.setBounds(90, 198, 113, 14);
		panel.add(lblLocalidad);

		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(90, 229, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblFechaCumple = new JLabel("Fecha de cumpleaños:");
		lblFechaCumple.setBounds(92, 257, 131, 14);
		panel.add(lblFechaCumple);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setBounds(90, 291, 113, 14);
		panel.add(lblTelefono);
		
		JLabel lblTipoContacto = new JLabel("Tipo de contacto:");
		lblTipoContacto.setBounds(90, 321, 133, 17);
		panel.add(lblTipoContacto);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(249, 11, 210, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(249, 43, 210, 20);
		panel.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(249, 74, 210, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(249, 105, 210, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(249, 136, 210, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDpto = new JTextField();
		txtDpto.setBounds(249, 164, 210, 20);
		panel.add(txtDpto);
		txtDpto.setColumns(10);
		
		localidad = new JComboBox<String>();
		localidad.setBounds(249, 195, 164, 20);
		panel.add(localidad);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(249, 226, 210, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		fechaCumple = new JDateChooser();
		fechaCumple.setBackground(Color.RED);
		fechaCumple.setLocale(new Locale("es"));
		fechaCumple.setDateFormatString("yyyy-MM-dd");
		fechaCumple.getJCalendar().setMaxSelectableDate(new Date());
		fechaCumple.setBounds(249, 257, 210, 20);
		panel.add(fechaCumple);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(249, 288, 210, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		tipoContacto = new JComboBox<String>();
		tipoContacto.setBounds(249, 319, 164, 20);
		panel.add(tipoContacto);
		
		btnAgregarPersona = new JButton("Añadir contacto");
		btnAgregarPersona.setBounds(209, 396, 138, 28);
		panel.add(btnAgregarPersona);
		
		btnAgregarLocalidad = new JButton("+");
		btnAgregarLocalidad.setBounds(418, 195, 41, 20);
		panel.add(btnAgregarLocalidad);
		
		btnAgregarEtiqueta = new JButton("+");
		btnAgregarEtiqueta.setBounds(418, 318, 41, 20);
		panel.add(btnAgregarEtiqueta);
		
		btnCerrar = new JButton("Cerrar");
		btnCerrar.setBounds(426, 418, 89, 23);
		btnCerrar.addActionListener(e -> dispose());
		panel.add(btnCerrar);
		
		this.setVisible(false);
	}
	
	public void cargarLocalidades(List<LocalidadDTO> localidades)
	{
		this.localidad.removeAllItems();
		for(LocalidadDTO l : localidades) 
		{
			this.localidad.addItem(l.getLocalidad());
		}
	}
	
	public void cargarTiposDeContacto(List<TipoContactoDTO> tipoDeContactos)
	{
		this.tipoContacto.removeAllItems();
		for(TipoContactoDTO t : tipoDeContactos) 
		{
			this.tipoContacto.addItem(t.getTipoContacto());
		}
	}
	
	public ArrayList<String> camposInvalidos() {
		campos = new ArrayList<>();
		validador = new ValidadorInput();
		if(! validador.validarTextoSinEspacios(txtNombre.getText())) campos.add("Nombre");
		if(! validador.validarTextoSinEspacios(txtApellido.getText())) campos.add("Apellido");
		if(! validador.validarTelefono(txtTelefono.getText())) campos.add("Teléfono");
		if(! validador.validarEmail(txtEmail.getText())) campos.add("Email");
		if(! validador.validarTextoConEspacios(txtCalle.getText())) campos.add("Calle");
		if(! validador.validarNumerico(txtAltura.getText())) campos.add("Altura");
		if(! validador.validarNumericoVacio(txtPiso.getText())) campos.add("Piso");//Acepta que el campo esté vacío
		if(! validador.validarAlfaNumericoVacio(txtDpto.getText())) campos.add("Depto"); //Acepta que el campo esté vacío
		return campos;
	}
	
	public String mensajeVerificarCampos() {
		String texto = "Por favor revise los campos: ";
		for (int i = 0; i < campos.size(); i++) {
			if(campos.size() == 1) texto = "Por favor revise el campo " + campos.get(i);
			else {
				if(i == campos.size()-1) texto += campos.get(i) + ".";
				else texto += campos.get(i) + ", ";
			}
		}
		return texto;
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public JTextField getTxtApellido() 
	{
		return txtApellido;
	}

	public JTextField getTxtCalle()
	{
		return txtCalle;
	}

	public JTextField getTxtAltura()
	{
		return txtAltura;
	}

	public JTextField getTxtPiso()
	{
		return txtPiso;
	}

	public JTextField getTxtDpto()
	{
		return txtDpto;
	}

	public JTextField getTxtEmail()
	{
		return txtEmail;
	}

	public String getTxtLocalidad()
	{
		return (String) localidad.getSelectedItem();
	}

	public String getTxtTipoContacto()
	{
		return (String) tipoContacto.getSelectedItem();
	}

	public Date getTxtCumple()
	{
		return fechaCumple.getDate();
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public JButton getBtnAgregarLocalidad() 
	{
		return btnAgregarLocalidad;
	}
	
	public JButton getBtnAgregarEtiqueta() 
	{
		return btnAgregarEtiqueta;
	}
	
	public JButton getBtnCerrar() 
	{
		return btnCerrar;
	}
	
	public void cerrar()
	{
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtApellido.setText(null);
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtPiso.setText(null);
		this.txtDpto.setText(null);
		this.txtEmail.setText(null);
		this.dispose();
	}
}