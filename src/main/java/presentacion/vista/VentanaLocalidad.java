package presentacion.vista;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.LocalidadDTO;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaLocalidad extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private static VentanaLocalidad INSTANCE;
	private JScrollPane spLocalidades;
	private JButton btnNueva;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JTable tablaLocalidades;
	private DefaultTableModel modelLocalidades;
	private  String[] localidadColumna = {"Localidad"};

	
	public static VentanaLocalidad getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaLocalidad(); 	
			return new VentanaLocalidad();
		}
		else
			return INSTANCE;
	}
	
	private VentanaLocalidad() 
	{
		super();
		setTitle("Localidad");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 480, 348);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		spLocalidades = new JScrollPane();
		spLocalidades.setBounds(50, 27, 350, 178);
		contentPane.add(spLocalidades);
		
		modelLocalidades = new DefaultTableModel(null,localidadColumna);
		tablaLocalidades = new JTable(modelLocalidades);
		
		tablaLocalidades.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaLocalidades.getColumnModel().getColumn(0).setResizable(false);
		
		spLocalidades.setViewportView(tablaLocalidades);
		
		btnNueva = new JButton("Nueva");
		btnNueva.setBounds(50, 230, 89, 23);
		contentPane.add(btnNueva);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(182, 230, 89, 23);
		contentPane.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBorrar.setBounds(311, 230, 89, 23);
		contentPane.add(btnBorrar);
		
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void llenarTablaLocalidades(List<LocalidadDTO> localidadesEnTabla)
	{
		this.getModelLocalidades().setRowCount(0);
		this.getModelLocalidades().setColumnCount(0);
		this.getModelLocalidades().setColumnIdentifiers(this.getNombreLocalidadColumna());

		for (LocalidadDTO l : localidadesEnTabla)
		{
			String localidad = l.getLocalidad();
			
			Object[] fila = {localidad};
			this.getModelLocalidades().addRow(fila);
		}
	}

	private String[] getNombreLocalidadColumna() {
		
		return this.localidadColumna;
	}

	private DefaultTableModel getModelLocalidades() {
		
		return this.modelLocalidades;
	}

	public JButton getBtnNueva() 
	{
		return btnNueva;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JTable getTablaLocalidades()
	{
		return tablaLocalidades;
	}


}