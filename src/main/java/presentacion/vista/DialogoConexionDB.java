package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;

public class DialogoConexionDB extends JDialog {
	private static final long serialVersionUID = 1L;
	private static DialogoConexionDB INSTANCIA;
	private JPanel contentPane;
	private JTextField textURL;
	private JTextField textUSER;	
	private JTextField textPASS;
	private JTextField txtPuerto;
	private JTextField textNombreDB;
	private JButton btnAceptar;
	private JButton btnSalir;
	
	public static DialogoConexionDB getInstance(){
		if(INSTANCIA == null)
			return new DialogoConexionDB();
		else
			return INSTANCIA;
	}

	private DialogoConexionDB(){
		setTitle("Conexion con base de datos");		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 437, 393);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 411, 342);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblURL = new JLabel("URL");
		lblURL.setBounds(25, 73, 64, 14);
		panel.add(lblURL);

		textURL = new JTextField();
		textURL.setBounds(211, 70, 164, 20);
		panel.add(textURL);
		textURL.setColumns(10);
		textURL.setToolTipText("localhost");

		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(25, 165, 64, 14);
		panel.add(lblUsuario);

		textUSER = new JTextField();
		textUSER.setBounds(211, 162, 164, 20);
		panel.add(textUSER);
		textUSER.setColumns(10);
		textUSER.setToolTipText("root");

		JLabel lblPassword = new JLabel("Contraseña:");
		lblPassword.setBounds(25, 199, 110, 14);
		panel.add(lblPassword);

		textPASS = new JTextField();
		textPASS.setBounds(211, 193, 164, 20);
		panel.add(textPASS);
		textPASS.setColumns(10);
		textPASS.setToolTipText("password");
				
		txtPuerto = new JTextField();
		txtPuerto.setToolTipText("root");
		txtPuerto.setColumns(10);
		txtPuerto.setBounds(211, 101, 164, 20);
		panel.add(txtPuerto);
		txtPuerto.setToolTipText("3306");
		
		JLabel lblPuerto = new JLabel("Puerto:");
		lblPuerto.setBounds(25, 104, 64, 14);
		panel.add(lblPuerto);
		
		textNombreDB = new JTextField();
		textNombreDB.setToolTipText("root");
		textNombreDB.setColumns(10);
		textNombreDB.setBounds(211, 132, 164, 20);
		panel.add(textNombreDB);
		textNombreDB.setToolTipText("GRUPO_:n°");
		
		JLabel lblNombreBaseDe = new JLabel("Nombre base de datos:");
		lblNombreBaseDe.setBounds(25, 135, 171, 14);
		panel.add(lblNombreBaseDe);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(250, 278, 89, 23);
		panel.add(btnAceptar);
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(70, 278, 89, 23);
		btnSalir.addActionListener(e -> dispose());
		panel.add(btnSalir);
		
		JLabel lblIngreseLasCredenciales = new JLabel("Ingrese las credenciales para conectar con la base de datos");
		lblIngreseLasCredenciales.setBounds(8, 11, 393, 51);
		panel.add(lblIngreseLasCredenciales);
		
		agregarValidaciones(); 
	}
	
	private void agregarValidaciones() {

	}
	
	public void mostrarVentana()	{
		this.setVisible(true);
	}
	
	public void cerrar() {
		dispose();
	}
	
	public String getURL()	{
		return textURL.getText();
	}

	public String getUSER()	{
		return textUSER.getText();
	}

	public String getPASS()	{
		return textPASS.getText();
	}
	
	public String getNombreDB()	{
		return textNombreDB.getText();
	}
	public String getPuerto() {
		return txtPuerto.getText();
	}
	
	public JButton getBtnAceptar(){
		return btnAceptar;
	}
}