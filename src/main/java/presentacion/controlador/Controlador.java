package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.DialogoEditarLocalidad;
import presentacion.vista.DialogoEditarTipoContacto;
import presentacion.vista.DialogoLocalidad;
import presentacion.vista.DialogoTipoContacto;
import presentacion.vista.VentanaEditarPersona;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VentanaTipoContacto;
import presentacion.vista.Vista;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private List<LocalidadDTO> localidadesEnTabla;
		private List<TipoContactoDTO> tipoContactoEnTabla;
		private VentanaPersona ventanaPersona; 
		private VentanaEditarPersona ventanaEditarPersona;
		private VentanaLocalidad ventanaLocalidad;
		private VentanaTipoContacto ventanaTipoContacto;
		private DialogoLocalidad dialogoLocalidad;
		private DialogoEditarLocalidad dialogoEditarLocalidad;
		private DialogoEditarTipoContacto dialogoEditarTipoContacto;
		private DialogoTipoContacto dialogoTipoContacto;
	//	private DialogoConexionDB dialogoConexionDB;
		
		private Agenda agenda;
		
		public Controlador(Vista vista, Agenda agenda)
		{
			//vista principal
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnEditar().addActionListener(e->ventanaEditarPersona(e));
			
			//Reporte
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			
			//acciones de la para agregar persona
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersonaNueva(p));
			this.ventanaPersona.getBtnAgregarLocalidad().addActionListener(al->ventanaLocalidad(al));
			this.ventanaPersona.getBtnAgregarEtiqueta().addActionListener(t->ventanaTipoContacto(t));
			
			//acciones de edicion para persona
			this.ventanaEditarPersona = VentanaEditarPersona.getInstance();
			this.ventanaEditarPersona.getbtnGuardarPersona().addActionListener(gp->guardarPersonaEditada(gp));
			this.ventanaEditarPersona.getBtnAgregarLocalidad().addActionListener(al->ventanaLocalidad(al));
			this.ventanaEditarPersona.getBtnAgregarEtiqueta().addActionListener(t->ventanaTipoContacto(t));

			//acciones ventana  localidades
			this.ventanaLocalidad = VentanaLocalidad.getInstance();
			this.ventanaLocalidad.getBtnNueva().addActionListener(nl->dialogoLocalidad(nl));
			this.ventanaLocalidad.getBtnEditar().addActionListener(el->dialogoEditarLocalidad(el));
			this.ventanaLocalidad.getBtnBorrar().addActionListener(bl->borrarLocalidad(bl));
			
			//Acciones ventana  tipoContacto
			this.ventanaTipoContacto = VentanaTipoContacto.getInstance();
			this.ventanaTipoContacto.getBtnNuevo().addActionListener(nt->dialogoTipoContacto(nt));
			this.ventanaTipoContacto.getBtnEditar().addActionListener(et->dialogoEditarTipoContacto(et));
			this.ventanaTipoContacto.getBtnBorrar().addActionListener(bt->borrarTipoContacto(bt));
			
			//Acciones para editar localidad
			this.dialogoLocalidad = DialogoLocalidad.getInstance();
			this.dialogoEditarLocalidad = DialogoEditarLocalidad.getInstance();
			this.dialogoLocalidad.getBtnGuardarLocalidad().addActionListener(gl->guardarLocalidad(gl));
			this.dialogoEditarLocalidad.getBtnGuardarLocalidad().addActionListener(gle->guardarLocalidadEditada(gle));
			
			//Acciones para editar tipoContacto
			this.dialogoTipoContacto = DialogoTipoContacto.getInstance();
			this.dialogoEditarTipoContacto = DialogoEditarTipoContacto.getInstance();
			this.dialogoTipoContacto.getBtnGuardarTipoContacto().addActionListener(gt->guardarTipoContacto(gt));
			this.dialogoEditarTipoContacto.getBtnGuardarTipoContacto().addActionListener(gte->guardarTipoContactoEditado(gte));
	
			//Coexion base de datos
			//this.dialogoConexionDB = DialogoConexionDB.getInstance();
			
			this.agenda = agenda;
		}

		private void ventanaAgregarPersona(ActionEvent a) {
			
			this.ventanaPersona.cargarLocalidades(this.agenda.obtenerLocalidades());
			this.ventanaPersona.cargarTiposDeContacto(this.agenda.obtenerTipoContacto());
			
			this.ventanaPersona.mostrarVentana();
		}

		private void guardarPersonaNueva(ActionEvent p) {
			
			if(this.ventanaPersona.camposInvalidos().size() == 0) {
				String nombre = this.ventanaPersona.getTxtNombre().getText();
				String telefono = this.ventanaPersona.getTxtTelefono().getText();
				String apellido = this.ventanaPersona.getTxtApellido().getText();
				String calle = this.ventanaPersona.getTxtCalle().getText();
				int altura = Integer.parseInt(this.ventanaPersona.getTxtAltura().getText());
				int piso = Integer.parseInt(this.ventanaPersona.getTxtPiso().getText());
				String depto = this.ventanaPersona.getTxtDpto().getText();			
				String localidad = this.ventanaPersona.getTxtLocalidad();
				String correo = this.ventanaPersona.getTxtEmail().getText();
				Date fechaNaciemiento = this.ventanaPersona.getTxtCumple();
				String tipoContacto = this.ventanaPersona.getTxtTipoContacto();
			
				PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, apellido, telefono, calle, altura, piso, depto, localidad, correo, fechaNaciemiento, tipoContacto );
				this.agenda.agregarPersona(nuevaPersona);
				this.refrescarTabla();
				this.ventanaPersona.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.ventanaPersona.mensajeVerificarCampos());
			}
		} 
		
		private void guardarPersonaEditada(ActionEvent gp) {

			if(this.ventanaEditarPersona.camposInvalidos().size() == 0) 
			{
				String nombre = this.ventanaEditarPersona.getTxtNombre().getText();
				String telefono = this.ventanaEditarPersona.getTxtTelefono().getText();
				String apellido = this.ventanaEditarPersona.getTxtApellido().getText();
				String calle = this.ventanaEditarPersona.getTxtCalle().getText();
				int altura = Integer.parseInt(this.ventanaEditarPersona.getTxtAltura().getText());
				int piso = Integer.parseInt(this.ventanaEditarPersona.getTxtPiso().getText());
				String depto = this.ventanaEditarPersona.getTxtDpto().getText();			
				String localidad = this.ventanaEditarPersona.getTxtLocalidad();
				String correo = this.ventanaEditarPersona.getTxtEmail().getText();
				Date fechaNaciemiento = this.ventanaEditarPersona.getTxtCumple();
				String tipoContacto = this.ventanaEditarPersona.getTxtTipoContacto();
				
				this.agenda.borrarPersona(this.personasEnTabla.get(this.vista.getTablaPersonas().getSelectedRow()));
				
				PersonaDTO PersonaModificada = new PersonaDTO(0, nombre, apellido, telefono, calle, altura, piso, depto, localidad, correo, fechaNaciemiento, tipoContacto );
				this.agenda.agregarPersona(PersonaModificada);
				this.refrescarTabla();
				this.ventanaEditarPersona.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.ventanaEditarPersona.mensajeVerificarCampos());
			}
		} 
		
		private void guardarLocalidad(ActionEvent gl) {
			if(this.dialogoLocalidad.camposInvalidos().size() == 0) 
			{
				String localidad = this.dialogoLocalidad.getLocalidad().getText();
				
				LocalidadDTO nuevaLocalidad = new LocalidadDTO(0, localidad);
				this.agenda.agregarNuevaLocalidad(nuevaLocalidad);
				this.refrescarTablaLocalidades();
				this.dialogoLocalidad.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.dialogoLocalidad.mensajeVerificarCampos());
			}
		}
		
		private void guardarTipoContacto(ActionEvent gt) {

			if(this.dialogoTipoContacto.camposInvalidos().size() == 0) 
			{
				String tipoContacto = this.dialogoTipoContacto.getTipoContacto().getText();
				
				TipoContactoDTO nuevoTipoContacto = new TipoContactoDTO(0, tipoContacto);
				this.agenda.agregarNuevoTipoDeContacto(nuevoTipoContacto);
				this.refrescarTablaTipoContacto();
				this.dialogoTipoContacto.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.dialogoTipoContacto.mensajeVerificarCampos());
			}
		}

		private void guardarLocalidadEditada(ActionEvent gle) {
			if(this.dialogoEditarLocalidad.camposInvalidos().size() == 0) 
			{
				String localidad = this.dialogoEditarLocalidad.getLocalidad().getText();
				
				this.agenda.borrarLocalidad(this.localidadesEnTabla.get(this.ventanaLocalidad.getTablaLocalidades().getSelectedRow()));
				
				LocalidadDTO localidadModificada = new LocalidadDTO(0, localidad);
				this.agenda.agregarNuevaLocalidad(localidadModificada);
				this.refrescarTablaLocalidades();
				this.dialogoEditarLocalidad.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.ventanaPersona.mensajeVerificarCampos());
			}
		}
		
		private void guardarTipoContactoEditado(ActionEvent gte) {

			if(this.dialogoEditarTipoContacto.camposInvalidos().size() == 0) 
			{
				String tipoContacto = this.dialogoEditarTipoContacto.getTipoContacto().getText();
				
				this.agenda.borrarTipoDeContacto(this.tipoContactoEnTabla.get(this.ventanaTipoContacto.getTablaTipoContacto().getSelectedRow()));
				
				TipoContactoDTO tipoContactoModificado = new TipoContactoDTO(0, tipoContacto);
				this.agenda.agregarNuevoTipoDeContacto(tipoContactoModificado);
				this.refrescarTablaTipoContacto();
				this.dialogoEditarTipoContacto.cerrar();
			}else {
				JOptionPane.showMessageDialog(null, this.dialogoEditarTipoContacto.mensajeVerificarCampos());
			}
		}

		private void mostrarReporte(ActionEvent r) {
			
			ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas());
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void borrarLocalidad(ActionEvent bl)
		{
			try {
				int[] filasSeleccionadas = this.ventanaLocalidad.getTablaLocalidades().getSelectedRows();
				for (int fila : filasSeleccionadas)
				{
					this.agenda.borrarLocalidad(this.localidadesEnTabla.get(fila));
				}
			} catch(Exception e){
				lanzarMensajeErrorBorrar("No se puede eliminar. La localidad esta en uso.");
				this.refrescarTablaLocalidades();
			}
			this.refrescarTablaLocalidades();
		}
		
		public void borrarTipoContacto(ActionEvent bt)
		{
			try {
				int[] filasSeleccionadas = this.ventanaTipoContacto.getTablaTipoContacto().getSelectedRows();
				for (int fila : filasSeleccionadas)
				{
					this.agenda.borrarTipoDeContacto(this.tipoContactoEnTabla.get(fila));
				}
				
				this.refrescarTablaTipoContacto();
			}catch(Exception e){

				lanzarMensajeErrorBorrar("No se puede eliminar. La etiqueta esta en uso.");
				this.refrescarTablaTipoContacto();;
			}
			this.refrescarTablaTipoContacto();
		}
		
		private void ventanaEditarPersona(ActionEvent e)
		{
			if(this.vista.getTablaPersonas().getSelectedRow()>=0)
			{
				
				PersonaDTO datosPersona = this.personasEnTabla.get(this.vista.getTablaPersonas().getSelectedRow());
				
				this.ventanaEditarPersona.precargarDatos(datosPersona.getNombre(), datosPersona.getApellido(), datosPersona.getCalle(), datosPersona.getAltura(), datosPersona.getPiso(), datosPersona.getDepto(), datosPersona.getLocalidad(), datosPersona.getCorreo(), datosPersona.getFechaNacimiento(), datosPersona.getTipoContacto(), datosPersona.getTelefono());
				this.ventanaEditarPersona.cargarLocalidades(this.agenda.obtenerLocalidades());
				this.ventanaEditarPersona.cargarTiposDeContacto(this.agenda.obtenerTipoContacto());
				
				this.ventanaEditarPersona.mostrarVentana();
			}else {
				lanzarMensajeErrorEditar();
			}
		}
		
		private void dialogoEditarLocalidad(ActionEvent el)
		{
			if(this.ventanaLocalidad.getTablaLocalidades().getSelectedRow()>=0)
			{
				LocalidadDTO datosLocalidad = this.localidadesEnTabla.get(this.ventanaLocalidad.getTablaLocalidades().getSelectedRow());
				
				this.dialogoEditarLocalidad.precargarDatos(datosLocalidad.getLocalidad());
				
				this.dialogoEditarLocalidad.mostrarVentana();
			}else {
				lanzarMensajeErrorEditar();
			}
		}
		
		private void dialogoEditarTipoContacto(ActionEvent et)
		{
			if(this.ventanaTipoContacto.getTablaTipoContacto().getSelectedRow()>=0)
			{
				TipoContactoDTO datoTipoContacto = this.tipoContactoEnTabla.get(this.ventanaTipoContacto.getTablaTipoContacto().getSelectedRow());
				
				this.dialogoEditarTipoContacto.precargarDatos(datoTipoContacto.getTipoContacto());
				
				this.dialogoEditarTipoContacto.mostrarVentana();
			}else {
				lanzarMensajeErrorEditar();
			}
		}
	
		public void inicializar()
		{
			this.refrescarTabla();
			this.refrescarTablaLocalidades();
			this.refrescarTablaTipoContacto();
			this.vista.show();
		}
		
		private void refrescarTablaTipoContacto() 
		{
			this.tipoContactoEnTabla = agenda.obtenerTipoContacto();
			this.ventanaTipoContacto.llenarTablaTipoContacto(this.tipoContactoEnTabla);
			
		}

		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			this.vista.llenarTabla(this.personasEnTabla);
		}
		
		private void refrescarTablaLocalidades()
		{
			this.localidadesEnTabla = agenda.obtenerLocalidades();
			this.ventanaLocalidad.llenarTablaLocalidades(this.localidadesEnTabla);
		}
		
		private void ventanaLocalidad(ActionEvent al)
		{
			this.ventanaLocalidad.mostrarVentana();
		}
	
		private void ventanaTipoContacto(ActionEvent t) {
			this.ventanaTipoContacto.mostrarVentana();
		}
		
		private void dialogoLocalidad(ActionEvent al)
		{
			this.dialogoLocalidad.mostrarVentana();
		}
		
		private void dialogoTipoContacto(ActionEvent nt)
		{
			this.dialogoTipoContacto.mostrarVentana();
		}
		
		/*private void dialogodialogoConexionDB(ActionEvent nt)
		{
			this.dialogoConexionDB.mostrarVentana();
		}*/
		
		@Override
		public void actionPerformed(ActionEvent e) { }
		
		 public static void lanzarMensajeErrorEditar()
		 {
			 //Lanzamos el mensaje:
			 JOptionPane.showMessageDialog(null, "Debe seleccionar un elemento para poder editar");
		 }
		 
		 public static void lanzarMensajeErrorBorrar(String string)
		 {
			 //Lanzamos el mensaje:
			 JOptionPane.showMessageDialog(null, string);
		 }
		 

}
