package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import persistencia.conexion.Conexion;

import javax.swing.JOptionPane;

import presentacion.vista.DialogoConexionDB;

public class ControaldorConexionDB implements ActionListener{

	private  DialogoConexionDB dialogo;
	private  Controlador controlador;
	private boolean cargandoDatos;
	

	public ControaldorConexionDB(Controlador controlador){	
		
		this.controlador = controlador;
		this.dialogo = DialogoConexionDB.getInstance();
		cargarActionListeners();
	}
	
	public void inicializar () {
		boolean conectable = Conexion.testConexion();
		if(!conectable)
		{
			JOptionPane.showOptionDialog(
				  	null, "Coloque las credenciales correctas para conectarse a la base.", 
		             "Confirmacion", JOptionPane.CANCEL_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
			while(!conectable) {
				this.dialogo.mostrarVentana();
				if(!Conexion.testConexion()) {
					cargandoDatos = false;
					while(!cargandoDatos) {
						System.out.println("");
						// wait for the user input
					}
				}else {
					conectable = true;
					this.dialogo.dispose();
				}
			}
			this.controlador.inicializar();
			
		}else {
			try {
				this.controlador.inicializar();
			}catch(Exception error) {
				Logger log = Logger.getLogger(Conexion.class);
				log.error("Conexión fallida", error);
				inicializar();
			}
		}
		
	}
	
	public void mostrarVentana() {
		dialogo.mostrarVentana();
	}
	
	public void cerrarVentana() {
		dialogo.cerrar();
	}
	
	public boolean dialogoCargado() {
		return dialogo != null;
	}
	
	private void cargarActionListeners() {		
		dialogo.getBtnAceptar().addActionListener(a->guardarConexionDB(a));
		dialogo.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e) {
				dialogo.cerrar();
				System.exit(0);
			}
		});
		
	}	

	private void guardarConexionDB(ActionEvent a)
	{
			try (OutputStream output = new FileOutputStream("Config.properties")){
				
				Properties p = new Properties();
				
				p.setProperty("URL", dialogo.getURL());
				p.setProperty("PUERTO", dialogo.getPuerto());
				p.setProperty("NOMBREBD", dialogo.getNombreDB());
				p.setProperty("USER", dialogo.getUSER());
				p.setProperty("PASS", dialogo.getPASS());
				
				p.store( output, null);
				this.dialogo.dispose();
				cargandoDatos = true;
				
			} catch(IOException ioe) {
				ioe.printStackTrace();
			}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}	

}
