package dto;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String calle;
	private int altura;
	private Integer piso;
	private String depto;
	private String localidad;
	private String correo;
	private Date fechaNacimiento;
	private String tipoContacto;
	private String apellido;

	public PersonaDTO(int idPersona, String nombre, String apellido, String telefono, String calle, int altura, Integer piso, String depto, String localidad, String correo, Date fechaNacimiento, String tipoContacto) {
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.calle = calle;
		this.altura = altura;
		this.piso = piso;
		this.depto = depto;
		this.localidad = localidad;
		this.correo = correo;
		this.fechaNacimiento = fechaNacimiento;
		this.tipoContacto = tipoContacto;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public Integer getPiso() {
		return piso;
	}

	public void setPiso(Integer piso) {
		this.piso = piso;
	}

	public String getDepto() {
		return depto;
	}

	public void setDepto(String depto) {
		this.depto = depto;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	//Para signo del zodiaco chino.
	@SuppressWarnings("deprecation")
	public String getZodiacoChino() {
		int año= obtenerAño();
		int resto = año % 12;
		String horoscopo="";
	        switch (resto) {
	            case 0: 
	            	horoscopo = "Mono"; break;
	            case 1: 
	            	horoscopo = "Gallo"; break;
	            case 2: 
	            	horoscopo = "Perro"; break;
	            case 3: 
	            	horoscopo = "Cerdo"; break;
	            case 4: 
	            	horoscopo = "Rata"; break;
	            case 5: 
	            	horoscopo = "Buey"; break;
	            case 6:
	            	horoscopo = "Tigre"; break;
	            case 7: 
	            	horoscopo = "Conejo"; break;
	            case 8: 
	            	horoscopo = "Dragon"; break;
	            case 9:
	            	horoscopo = "Serpiente"; break;
	            case 10:
	            	horoscopo = "Caballo"; break;
	            case 11:
	            	horoscopo = "Cabra"; break;
	        }
		return horoscopo;
	}

	private int obtenerAño() {
		    if (null == this.getFechaNacimiento()){
		        return 0;
		    }
		    else{
		        String formato = "yyyy";
		        SimpleDateFormat dateFormat = new SimpleDateFormat(formato);
		        return Integer.parseInt(dateFormat.format(this.getFechaNacimiento()));
		    }
		
	}
	
}
