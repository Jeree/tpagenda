package modelo;

import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private TipoContactoDAO tipoContacto;

	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.tipoContacto = metodo_persistencia.createTipoContactoDAO();

	}
	
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return ordenarTipoContacto(this.persona.readAll());		
	}
    //campos obligatorios, error editar (validar que nos ea FK)
	//jasper agregar campos tipo contacto(ordenar)
	//agrupar por zodiaco y mostrar total(porcentaje %)

	private List<PersonaDTO> ordenarTipoContacto(List<PersonaDTO> readAll)
	{
		List<PersonaDTO> ret = this.persona.readAll();
		Collections.sort(ret, SegunTipoContacto());
		Collections.sort(ret, segunNombreContacto());
		Collections.sort(ret, segunZodiacoChino());
		return ret;
	}
	
	
	private Comparator<PersonaDTO> segunZodiacoChino() {
		return (uno,otro) -> uno.getZodiacoChino().compareTo(otro.getZodiacoChino());
	}

	private Comparator<PersonaDTO> segunNombreContacto() {
		return (uno,otro)-> uno.getNombre().compareTo(otro.getNombre());
	}

	private Comparator<PersonaDTO> SegunTipoContacto() {	
		return (uno, otro)-> uno.getTipoContacto().compareTo(otro.getTipoContacto());
		
	}

	public void agregarNuevaLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}
	
	public void borrarLocalidad(LocalidadDTO localidadParaEliminar) 
	{
		this.localidad.delete(localidadParaEliminar);
	}
	
	public List<LocalidadDTO> obtenerLocalidades()
	{
		return this.localidad.readAll();
	}
	
	public void agregarNuevoTipoDeContacto(TipoContactoDTO nuevoTipoContacto)
	{
		this.tipoContacto.insert(nuevoTipoContacto);
	}

	public void borrarTipoDeContacto(TipoContactoDTO tipoContactoParaEliminar) 
	{
		this.tipoContacto.delete(tipoContactoParaEliminar);
	}
	
	public List<TipoContactoDTO> obtenerTipoContacto()
	{
		return this.tipoContacto.readAll();
	}
}
