package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoContactoDTO;

public interface TipoContactoDAO {
	
		public boolean insert(TipoContactoDTO tipoContacto);
	
		public boolean delete(TipoContactoDTO tipoContactoParaEliminar);
		
		public List<TipoContactoDTO> readAll();

}



