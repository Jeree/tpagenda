package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO {
		
		public boolean insert(LocalidadDTO localidad);

		public boolean delete(LocalidadDTO localidadParaEliminar);
		
		public List<LocalidadDTO> readAll();

}
