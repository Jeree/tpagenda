package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT "
											+ "INTO personas(idpersona,nombre,telefono,calle,altura,piso,departamento,idlocalidad,correo,fecha_Nacimiento,idTipoContacto,apellido) "
											+ "VALUES (?,?,?,?,?,?,?,(select idLocalidad from localidad where localidad = ?),?,?,"
											+ "(select idtipocontacto from tipo_contacto where tipo_contacto = ?),?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas p "
												+ "INNER JOIN localidad l ON p.idLocalidad = l.idLocalidad "
												+ "INNER JOIN tipo_contacto t ON p.idTipoContacto = t.idTipoContacto";
		
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getCalle());
			statement.setInt(5, persona.getAltura());
			statement.setInt(6, persona.getPiso());
			statement.setString(7, persona.getDepto());
			statement.setString(8, persona.getLocalidad());
			statement.setString(9, persona.getCorreo());
			Date fechaCumpleaños = new Date(persona.getFechaNacimiento().getTime());
			statement.setDate(10, fechaCumpleaños);
			//java.sql.Date sqlStartDate = new Date(persona.getFechaNacimiento().getTime());
			//statement.setDate(10, sqlStartDate);
			
			statement.setString(11, persona.getTipoContacto());
			statement.setString(12, persona.getApellido());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}

	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("Nombre");
		String telefono = resultSet.getString("Telefono");
		String calle = resultSet.getString("Calle");
		int altura = resultSet.getInt("Altura");
		int piso = resultSet.getInt("Piso");
		String depto = resultSet.getString("Departamento");
		String localidad = resultSet.getString("Localidad");
		String correo = resultSet.getString("Correo");
		Date fechaCumpleaños = resultSet.getDate("Fecha_Nacimiento");
		String tipoContacto = resultSet.getString("Tipo_Contacto");
		String apellido = resultSet.getString("Apellido");
		
		return new PersonaDTO(id, nombre, apellido, telefono, 
								calle, altura, piso, depto, localidad, correo,
								fechaCumpleaños, tipoContacto);
	}
}
