package persistencia.conexion;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

import modelo.Agenda;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.Controlador;
import presentacion.vista.Vista;

public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);	
	private static String URLdb;
	private static String NOMBREdb;
	private static String USUARIOdb;
	private static String PASSdb;
	private static String PUERTOdb;
	private boolean conectable;
	
	private Conexion()
	{
	/*	URL=localhost:
		USER=root
		PASS=root
		NOMBREBD=GRUPO_11?serverTimezone=UTC&serverTimezone=America/Argentina/Buenos_Aires
		PUERTO=3306*/	
		try {
			Properties p = new Properties();
			p.load(new FileReader("Config.properties"));
			
			URLdb= p.getProperty("URL");
			PUERTOdb= p.getProperty("PUERTO");
			NOMBREdb= p.getProperty("NOMBREBD");
			USUARIOdb =p.getProperty("USER");
			PASSdb =p.getProperty("PASS");
			
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
				
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver"); // quitar si no es necesario ejemplo jdbc:mysql://+url(localhost)+:+puerto+/+nombre, user , pass
			this.connection = DriverManager.getConnection("jdbc:mysql://"+URLdb+":"+PUERTOdb+"/"+NOMBREdb+"?serverTimezone=UTC&serverTimezone=America/Argentina/Buenos_Aires",USUARIOdb,PASSdb);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
			setConectable(true);
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
			setConectable(false);
		}
	}
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}

	public boolean getConectable() {
		return conectable;
	}

	public void setConectable(boolean conectable) {
		this.conectable = conectable;
	}
	
	public static boolean testConexion() {
		instancia = new Conexion();
		return instancia.getConectable();
	}
}
